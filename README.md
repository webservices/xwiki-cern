# XWiki

XWiki is a free wiki software platform written in Java with a design emphasis on extensibility. XWiki is an enterprise wiki. It includes WYSIWYG editing, OpenDocument based document import/export, semantic annotations, and tagging.

[XWiki.org's extension wiki](https://extensions.xwiki.org/) is home to XWiki extensions ranging from [code snippets](https://snippets.xwiki.org/) which can be pasted into wiki pages to loadable core modules. Many of XWiki Enterprise's features are provided by extensions which are bundled with it.

![logo](https://www.xwiki.org/xwiki/bin/view/Main/Logo?xpage=plain&act=svg&finput=logo-xwikiorange.svg&foutput=logo-xwikiorange.png&width=200)

## Table of contents

-   [Introduction](#introduction)
-   [Setup](#setup)
    -   [OpenID](#openid)
        -   [Configuration](#configuration)
        -   [Bypass OpenID Connect](#bypass-openID-connect)
        -   [Problems](#problems)
    -   [Mail](#mail)   
-   [Upgrading XWiki](#upgrading-xwiki)
-   [Details for the xwiki image](#details-for-the-xwiki-image)
    -   [Configuration Options](#configuration-options)
- [Bugs](#bugs)
-   [License](#license)

## Introduction

The goal is to provide a production-ready XWiki system running in Docker. This is why:

-	The OS is based on Debian and not on some smaller-footprint distribution like Alpine

## Setup

### OpenID

Install the plugin: OpenID Connect Authenticator (Version 1.15)

#### Configuration

[How to Configure your OIDC Application on CERN Keycloak](https://auth.docs.cern.ch/user-documentation/oidc/config/)

In the file 
> xwiki.cfg 

You have that add the authentication management class.

```
#-# The authentication management class.
xwiki.authentication.authclass=org.xwiki.contrib.oidc.auth.OIDCAuthServiceImpl
```

And in the file
> xwiki.properties

```
#-------------------------------------------------------------------------------------
# OpenId Connect
#-------------------------------------------------------------------------------------

#-# The OpenId Connect base URL of the XWiki instance to use as provider.#-# See following endpoints properties if the provider is not an XWiki instance.
#-# If not indicated, it will be asked to the user.
# oidc.xwikiprovider=
#-# The generic OpenId Connect endpoints to use to communicate with the provider.
#-# Not needed in case of XWiki based provider.
oidc.endpoint.authorization=https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth
oidc.endpoint.token=https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token
oidc.endpoint.userinfo=https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo

#-# The method used to access the userinfo endpoint.
#-#
#-# Supported values are:
#-# * GET: use GET HTTP method
#-# * POST: use POST HTTP method
#-#
#-# The default is:
# oidc.endpoint.userinfo.method=GET

#-# The pattern to use to generate the XWiki user name.
#-#
#-# The following variables are available:
#-# oidc.user.subject: the unique id of the user in the provider
#-# oidc.user.mail: the mail of the user
#-# oidc.user.familyName : the last name of the user
#-# oidc.user.givenName: the first name of the user
#-# oidc.user.preferredUsername: the recommended string to use as id for the user
#-# oidc.provider: the URL of the XWiki provider (only when a XWiki provider is used)
#-# oidc.provider.host: the host of the provider URL
#-# oidc.provider.path: the path of the provider URL
#-# oidc.provider.protocol: the protocol (usually https) of the provider URL
#-# oidc.provider.port: the port of the provider URL
#-# oidc.issuer: the issuer URI
#-# oidc.issuer.host: the host of the issuer URI
#-# oidc.issuer.path: the path of the issuer URI
#-# oidc.issuer.scheme: the scheme (usually https) of the issuer URI
#-# oidc.issuer.port: the port of the issuer URI
#-#
#-# The following suffixes can be used:
#-# * ".lowerCase": the lower case version of the string
#-# * ".upperCase": the upper case version of the string
#-# * ".clean": a version of the string stripped from ".", ":", ",", "@", "^" characters and "\s" (all forms of white spaces).
#-#             It can itself be suffixed with ".lowerCase" and ".uperCase".
#-##-# The default is:
oidc.user.nameFormater=${oidc.user.givenName}-${oidc.user.familyName}

#-# The pattern to use to generate the unique identifier of the user in the OpenId Connect provider.#-# It is used to avoid collisions with user have similar name.
#-#
#-# The syntax is the same than the one described for oidc.user.nameFormater property.
#-#
#-# The default is:
# oidc.user.subjectFormater=${oidc.user.cern_upn}

#-# The OpenID Connect client identifier used by the authenticator.
#-#
#-# The default is the automatically generated unique id of the XWiki instance.
# oidc.idtokenclaims=xwiki_instance_id

#-# The name of the claim used to get the list of group the user belong to
#-#
#-# The default is:
# oidc.groups.claim=xwiki_groups

#-# The custom claims to request to the provider for the UserInfo
#-#
#-# The available custom claims are:
#-# xwiki_groups (or whatever you indicated in oidc.groups.claim): the groups a user belong to in the provider (see "Group synchronization" section for more details)
#-# xwiki_user_<fieldname>: the suffix to use to request any field in the user profile document (generally when the provider is XWiki)
#-# The default is:
# oidc.userinfoclaims=xwiki_user_accessibility,xwiki_user_company,xwiki_user_displayHiddenDocuments,xwiki_user_editor,xwiki_user_usertype

#-# The time after which the user information should be refreshed (in milliseconds)
#-#
#-# The default is:
# oidc.userinforefreshrate=600000

#-# The client identifier used by the authentication.
#-# The default is the identifier of the XWiki instance.
oidc.clientid=REPLACEWITHYOURCLIENTID

#-# The client secret (optionally) registered on the provider.
#-# By default nothing is sent to the provider.
oidc.secret=REPLACEWITHYOURSECRET

#-# How to send the client id and secret.
#-#
#-# Supported values are:
#-# * client_secret_basic: the id and the secret are sent using BASIC auth header
#-# * client_secret_post: the id and the secret are sent in the the request body
#-#
#-# The default is:
# oidc.endpoint.token.auth_method=client_secret_basic

#-# Receiving a groups list is enough to enable group synchronization but you might need to configure XWiki groups names different from the remote groups names.
#-#
# oidc.groups.mapping=MyXWikiGroup=my-oidc-group
# oidc.groups.mapping=MyXWikiGroup2=my-oidc-group2
# oidc.groups.mapping=MyXWikiGroup2=my-oidc-group3

#-# The groups the user need to belong to be allowed to authenticate.
#-# Not taken into account if not set or empty.
#-#
# oidc.groups.allowed=

#-# If the user belong to one of these groups it won't be allowed to authenticate
#-#
# oidc.groups.forbidden=

#-# Disable the OpenId Connect authenticator
#-#
#-# The default is:
# oidc.skipped=false
```

#### Bypass OpenID Connect
As indicated in the previous section you can disable OpenID Connect using the property oidc.skipped in the xwiki.properties file.

It's also possible to skip OpenId Connect temporarily using a URL parameter: for example https://mydomain/xwiki/bin/view/Main/?oidc.skipped=true

### Mail

> xwiki.properties

```
#-------------------------------------------------------------------------------------
# Mail
#-------------------------------------------------------------------------------------

#-# [Since 6.1M2]
#-# SMTP host when sending emails, defaults to "localhost".
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "smtp_server" property name.
mail.sender.host = smtp.cern.ch

#-# [Since 6.1M2]
#-# SMTP port when sending emails, defaults to 25.
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "smtp_port" property name.
mail.sender.port = 25

#-# [Since 6.1M2]
#-# From email address to use. Not defined by default and needs to be set up when calling the mail API.
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "admin_email" property name.
mail.sender.from = <<REPLACE_SENDER>>

#-# [Since 6.1M2]
#-# Username to authenticate on the SMTP server, if needed. By default no authentication is performed.
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "smtp_server_username"
#-# property name.
mail.sender.username = <<REPLACE_USER>>

#-# [Since 6.1M2]
#-# Password to authenticate on the SMTP server, if needed. By default no authentication is performed.
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "smtp_server_password"
#-# property name.
mail.sender.password = <<REPLACE_PASSWORD>>

#-# [Since 6.1M2]
#-# Extra Java Mail options (see https://javamail.java.net/nonav/docs/api/).
#-# This configuration property can be overridden in XWikiPreferences objects, by using the "javamail_extra_props"
#-# property name.
#-# By default the following properties are set automatically:
#-#   mail.transport.protocol = smtp
#-#   mail.smtp.host = <value of the mail.sender.host property>
#-#   mail.smtp.port = <value of the mail.sender.port property>
#-#   mail.smtp.user = <value of the mail.sender.username property>
#-#   mail.smtp.from = <value of the mail.sender.from property>
#-# Example:
mail.sender.properties = mail.smtp.starttls.enable = true
# mail.sender.properties = mail.smtp.socks.host = someserver

#-# [Since 6.4M2]
#-# Defines which authorization checks are done when sending mails using the Mail Sender Script Service.
#-# Example of valid values:
#-# - "programmingrights": the current document must have Programming Rights
#-# - "alwaysallow": no check is performed. This is useful when running XWiki in a secure environment where we
#-#   want to allow all users to be able to send emails through the Script Service.
#-# The default is:
# mail.sender.scriptServiceCheckerHint = programmingrights

#-# [Since 6.4M2]
#-# optional default email addresses to add to the BCC mail header when sending email.
# mail.sender.bcc = john@doe.com,mary@doe.com

#-# [Since 6.4RC1]
#-# The delay to wait between each mail being sent, in milliseconds. This is done to support mail throttling and not
#-# be considered a spammer by mail servers.
#-# The default is 8 seconds:
# mail.sender.sendWaitTime = 8000

#-# [Since 6.4.1, 7.0M1]
#-# When using the Database Mail Listener, whether mail statuses for mails that have been sent successfully must be
#-# discarded or not. They could be kept for tracability purpose for example.
#-# The default is:
mail.sender.database.discardSuccessStatuses = false

#-# [Since 11.6RC1]
#-# Max queue size for the prepare mail thread. When the max size is reached, asynchronously sending a mail will block
#-# till the first mail item in the prepare queue has been processed.
# mail.sender.prepareQueueCapacity = 1000

#-# [Since 11.6RC1]
#-# Max queue size for the send mail thread. When the max size is reached, the prepare queue will block till the first
# mail item in the send queue has been sent.
# mail.sender.sendQueueCapacity = 1000
```


## Upgrading XWiki
You've installed an XWiki docker image and used it and now comes the time when you'd like to upgrade XWiki to a newer version.

If you've followed the instructions above you've mapped the XWiki permanent directory to a local directory on your host.

All you need to do to upgrade is to stop the running XWiki container and start the new version of it that you want to upgrade to. You should keep your DB container running.

Note that your current XWiki configuration files (xwiki.cfg, xwiki.properties and hibernate.cfg.xml) will be preserved.

## Details for the xwiki image
### Configuration Options

The first time you create a container out of the xwiki image, a shell script (/usr/local/bin/docker-entrypoint.sh) is executed in the container to setup some configuration. The following environment variables can be passed:

DB_USER: The user name used by XWiki to read/write to the DB.
DB_PASSWORD: The user password used by XWiki to read/write to the DB.
DB_DATABASE: The name of the XWiki database to use/create.
DB_HOST: The name of the host (or docker container) containing the database. Default is "db".
INDEX_HOST: The hostname of an externally configured Solr instance. Defaults to "localhost", and configures an embedded Solr instance.
INDEX_PORT: The port used by an externally configured Solr instance. Defaults to 8983.
For the mail
EMAIL: You will need an CERN-email to use the mail feature on XWiki.
USER_EMAIL: CERN user to connect to server mail on CERN.
PASSWORD_EMAIL: Your password of your CERN Account to connect to server mail on CERN.

## Bugs

### Problem 1 
Problem with the emojis when you add in the text plain. 
### Solution
Changed the encoding from utf8 to utf8mb4 on the xwiki.cnf
[client]
        default-character-set = utf8mb4

        [mysqld]
        character-set-server = utf8mb4
        collation-server = utf8mb4_general_ci
        explicit_defaults_for_timestamp = 1

        [mysql]
        default-character-set = utf8mb4
### Problem 2
Insecure request (XMLHttpRequest endpoint over HTTP). 
### Solutiuon 
Added the new parameters: ProxyName, ProxyPort and Schema in the configuration of the Tomcat server on server.xml
<!-- A "Connector" represents an endpoint by which requests are received
              and responses are returned. Documentation at :
              Java HTTP Connector: /docs/config/http.html
              Java AJP  Connector: /docs/config/ajp.html
              APR (HTTP/AJP) Connector: /docs/apr.html
              Define a non-SSL/TLS HTTP/1.1 Connector on port 8080
          -->
          <Connector port="8080" protocol="HTTP/1.1"
                    connectionTimeout="20000"
                    redirectPort="8443"
                    proxyName="${APPLICATION_NAME}.web.cern.ch"
                    proxyPort="443"
                    scheme="https"/>
### Problem 3
Error when you try to create a new wiki within of the Xwiki created.
### Solution
It's necessary that the user of the database has permissions to create, update, delete, select and drop for any new database, you can obtain this using this command on MySQL.

`GRANT CREATE, SELECT, DELETE, INSERT, DROP, UPDATE ON *.* TO 'DATABASE_USERNAME';`

## License
XWiki is licensed under the LGPL 2.1.

The Dockerfile repository is also licensed under the LGPL 2.1.

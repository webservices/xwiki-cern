FROM tomcat:8-jdk11-adoptopenjdk-hotspot

#    ____  ____  ____      ____  _   __        _
#   |_  _||_  _||_  _|    |_  _|(_) [  |  _   (_)
#     \\ \\  / /    \\ \\  /\\  / /  __   | | / ]  __
#      > `' <      \\ \\/  \\/ /  [  |  | '' <  [  |
#    _/ /'`\\ \\_     \\  /\\  /    | |  | |`\\ \\  | |
#   |____||____|     \\/  \\/    [___][__|  \\_][___]

MAINTAINER Pablo Gonzalez <pablo.gonzalez.jimenez@cern.ch>

# Install LibreOffice + other tools
# Note that procps is required to get ps which is used by JODConverter to start LibreOffice
RUN apt-get update && apt-get --no-install-recommends -y install \
    curl \
    libreoffice \
    unzip \
    procps \
    vim \
    mysql-client \
    && rm -rf /var/lib/apt/lists/*


# Install XWiki as the ROOT webapp context in Tomcat
# Create the Tomcat temporary directory
# Configure the XWiki permanent directory
ARG XWIKI_VERSION=13.1
ENV XWIKI_VERSION		${XWIKI_VERSION}
ENV XWIKI_URL_PREFIX 		https://maven.xwiki.org/releases/org/xwiki/platform/xwiki-platform-distribution-war/${XWIKI_VERSION}
ARG XWIKI_DOWNLOAD_SHA256=885e41640d11cf8a3bb86333f0eb9c7836c138f346a632029402e388694dfa94
ENV XWIKI_DOWNLOAD_SHA256	${XWIKI_DOWNLOAD_SHA256}

ARG DB_HOST=${TEST_XWIKI_IT_DB_SERVICE_HOST}
ENV DB_HOST		${DB_HOST}
ARG DB_DATABASE=xwiki
ENV DB_DATABASE		${DB_DATABASE}
ARG DB_USER=
ENV DB_USER		${DB_USER} 
ARG DB_PASSWORD=
ENV DB_PASSWORD 	${DB_PASSWORD}

ARG INDEX_HOST=localhost
ENV INDEX_HOST          ${INDEX_HOST}
ARG INDEX_PORT=8983
ENV INDEX_PORT          ${INDEX_PORT}

# Parameters to configure the Mail on XWiki
#ARG EMAIL=replaceEmail
#ENV EMAIL				${EMAIL}
#ARG USER_EMAIL=replaceUser
#ENV USER_EMAIL				${USER_EMAIL}
#ARG PASSWORD_EMAIL=replacePassword
#ENV PASSWORD_EMAIL			${PASSWORD_EMAIL}

RUN rm -rf /usr/local/tomcat/webapps/* && \
 	mkdir -p /usr/local/tomcat/temp && \
	mkdir -p /usr/local/xwiki/data && \
        chmod 777 /usr/local/xwiki/data && \
 	curl -fSL "${XWIKI_URL_PREFIX}/xwiki-platform-distribution-war-${XWIKI_VERSION}.war" -o xwiki.war && \
 	echo "${XWIKI_DOWNLOAD_SHA256} xwiki.war" | sha256sum -c - && \
	unzip -d /usr/local/tomcat/webapps/ROOT xwiki.war && \
	rm -f xwiki.war

# Copy the JDBC driver in the XWiki webapp
#RUN cp /usr/share/java/mysql-connector-java-*.jar /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/
# For MYSQL, download the MySQL driver version from the Maven Central repository since there's no up to
# date Debian repository for it anymore.
ENV MYSQL_JDBC_VERSION="8.0.22"
ENV MYSQL_JDBC_SHA256="5019defbd12316295e97a6e88f2a9b07f118345a4e982710bba232e499b22f4f"
ENV MYSQL_JDBC_PREFIX="https://repo1.maven.org/maven2/mysql/mysql-connector-java/${MYSQL_JDBC_VERSION}"
ENV MYSQL_JDBC_ARTIFACT="mysql-connector-java-${MYSQL_JDBC_VERSION}.jar"
ENV MYSQL_JDBC_TARGET="/usr/local/tomcat/webapps/ROOT/WEB-INF/lib/${MYSQL_JDBC_ARTIFACT}"
RUN curl -fSL "${MYSQL_JDBC_PREFIX}/${MYSQL_JDBC_ARTIFACT}" -o $MYSQL_JDBC_TARGET && \
  echo "$MYSQL_JDBC_SHA256 $MYSQL_JDBC_TARGET" | sha256sum -c -

# Configure Tomcat. For example set the memory for the Tomcat JVM since the default value is too small for XWiki
COPY tomcat/setenv.sh /usr/local/tomcat/bin/
RUN chmod -R 777 /usr/local/tomcat

# Setup the XWiki Hibernate configuration
COPY xwiki/hibernate.cfg.xml /usr/local/tomcat/webapps/ROOT/WEB-INF/hibernate.cfg.xml

# Set a specific distribution id in XWiki for this docker packaging.
RUN sed -i 's/<id>org.xwiki.platform:xwiki-platform-distribution-war/<id>org.xwiki.platform:xwiki-platform-distribution-docker/' \
	/usr/local/tomcat/webapps/ROOT/META-INF/extension.xed

# Add scripts required to make changes to XWiki configuration files at execution time
# Note: we don't run CHMOD since 1) it's not required since the executabe bit is already set in git and 2) running
# CHMOD after a COPY will sometimes fail, depending on different host-specific factors (especially on AUFS).
COPY xwiki/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# OPENID
COPY xwiki/openid.cfg /usr/local/tomcat/temp/openid.cfg
COPY oidc/* /usr/local/tomcat/temp/oidc/

# Starts XWiki by starting Tomcat. All options passed to docker-entrypoint.sh. 
# If "xwiki" is passed then XWiki will be configured the first time the
# container executes and Tomcat will be started. If some other parameter is passed then it'll be executed to comply
# with best practices defined at https://github.com/docker-library/official-images#consistency.

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["xwiki"]

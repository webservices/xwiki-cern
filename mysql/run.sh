#!/bin/bash

    #verify the passed params
    echo 1 cmd arg : $1

    export user=$1

    #set the params ... Note the quotes ( needed for non-numeric values )
    mysql -uroot -e "set @user='${user}'; source run.sql;";

    #usage: bash run.sh my_db my_table